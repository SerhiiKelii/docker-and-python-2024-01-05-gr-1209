from .connection import conn
from psycopg import OperationalError


def execute_query(query: str, connection=conn):
    connection.autocommit = True
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        print("Query executed successfully.")
    except OperationalError as err:
        print(f"The error '{err}' occurred.")


create_message_table = """
CREATE TABLE IF NOT EXISTS message (
    id SERIAL PRIMARY KEY,
    message TEXT,
    userid BIGINT NOT NULL,
    message_time TIMESTAMP WITH TIME ZONE
)
"""


execute_query(create_message_table)

